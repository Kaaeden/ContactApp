package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.createcontactsapp.viewmodel.ContactsAppViewModel

class ContactDetails : Fragment() {
    private val contactListViewModel by activityViewModels<ContactsAppViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return ComposeView(requireContext()).apply {

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {

                val state = contactListViewModel.state.collectAsState()
                val contactIndex = arguments?.getInt("contactIndex")!!.toInt()
                val contactData = state.value.contactList[contactIndex]

                Column(modifier= Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .background(Color.DarkGray), horizontalAlignment = Alignment.CenterHorizontally){
                    Box(modifier = Modifier.fillMaxWidth(), Alignment.TopStart) {
                        Column(){
                        Row() {
                            Text("Name: ", fontSize = 20.sp)
                            Text(contactData.fName + contactData.lName, color = Color.White, fontSize = 20.sp)
                        }
                        Row() {
                            Text("Phone: ", fontSize = 20.sp)
                            Text(contactData.phone[0], color = Color.White, fontSize = 20.sp)
                        }
                        Row() {
                            Text("Email: ", fontSize = 20.sp)
                            Text(contactData.email[0], color = Color.White, fontSize = 20.sp)
                        }
                        Row() {
                            Text("Address: ", fontSize = 20.sp)
                            Text(
                                contactData.address.streetAddress + ", " +
                                        contactData.address.city + ", " +
                                        contactData.address.state + ", " +
                                        contactData.address.zipCode,
                                color = Color.White, fontSize = 20.sp
                            )
                        }
                        }
                    }
                    Button(modifier= Modifier.padding(15.dp), colors=ButtonDefaults.buttonColors(containerColor=Color.LightGray), onClick= {
                        val bundle: Bundle = Bundle()
                        bundle.putInt("contactIndex", contactIndex)
                        findNavController().navigate(R.id.contactEdit, bundle)}){Text("Edit Contact")}
                }

            }
        }
    }

}