package com.example.createcontactsapp.viewmodel

import com.example.createcontactsapp.model.local.Contact

data class ContactAppDataState(val isLoading: Boolean = false,
val contactList: MutableList<Contact> = mutableListOf(), val onError: String = ""){}
