package com.example.createcontactsapp.viewmodel

import androidx.lifecycle.ViewModel
import com.example.createcontactsapp.model.local.Address
import com.example.createcontactsapp.model.local.Contact
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class ContactsAppViewModel: ViewModel() {

    private val _state = MutableStateFlow(ContactAppDataState())
    val state get() = _state.asStateFlow()


    //TestContact
   // private val address1 = Address("2222 Noxville", "Whoville", "Mississippi", "07734")
    //private val contact1 = Contact("Kenneth", "Bolden", address1, listOf("8005882300"), listOf("kennethb@ravebizz.com"))

    fun updateContactList(contactInfo: Contact)
    {
        _state.update{it.copy(isLoading = true)}
        _state.update{ val cl_ = it.contactList
                        cl_.add( contactInfo)
                        it.copy(isLoading = false, contactList = cl_)}
        _state.update{it.copy(isLoading = false)}
    }
    fun updateContact(contactIndex: Int, newValue: Contact){

        _state.value.contactList.removeAt(contactIndex)
        _state.value.contactList.add(newValue)

    }

}