package com.example.createcontactsapp.model.local

data class Contact(val fName: String, val lName: String, val address:Address,
                   val phone: List<String>, val email: List<String>)