package com.example.createcontactsapp.model.local

data class Address(val streetAddress: String, val city: String, val state: String, val zipCode: String) {

}