package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.example.createcontactsapp.model.local.Address
import com.example.createcontactsapp.model.local.Contact
import com.example.createcontactsapp.viewmodel.ContactsAppViewModel
import com.example.createcontactsapp.views.ContactItemBox

class ContactsScreen1 : Fragment() {

    private val contactListViewModel by activityViewModels<ContactsAppViewModel>()
    //TestContact
    private val address1 = Address("2222 Noxville", "Whoville", "Mississippi", "07734")
    private val contact1 = Contact("Kenneth", "Bolden", address1, listOf("8005882300"), listOf("kennethb@ravebizz.com"))

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // contactListViewModel.updateContactList(contact1)

        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {

                val state = contactListViewModel.state.collectAsState()
                Column(
                    Modifier
                        .background(Color.Black)
                        .fillMaxWidth()
                        .fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally) {
                    LazyColumn() {

                        items(state.value.contactList) { contactItem ->
                            ContactItemBox(
                                contactDataItem = contactItem, goToDetails = {
                                    println("Contact Clicked")
                                    val bundle: Bundle = Bundle()
                                    bundle.putInt(
                                        "contactIndex",
                                        state.value.contactList.indexOf(contactItem)
                                    )
                                    println("Navigated")
                                    findNavController().navigate(R.id.contactDetails, bundle)

                                }
                            )
                        }
                    }//ButtonColors(containerColor=Color.Gray, contentColor =Color.White, disabledContentColor=Color.White, disabledContainerColor=Color.Gray)
                    Button(modifier=Modifier.padding(10.dp), colors = ButtonDefaults.buttonColors(containerColor = Color.Gray, contentColor = Color.White), onClick = { findNavController().navigate(R.id.contactCreate) }) { Text(" Create Contact ") }
                }


            }
        }
    }

}