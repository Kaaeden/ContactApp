package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.createcontactsapp.model.local.Address
import com.example.createcontactsapp.model.local.Contact
import com.example.createcontactsapp.viewmodel.ContactsAppViewModel


class ContactEdit : Fragment() {

    private val contactViewModel by activityViewModels<ContactsAppViewModel>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       return ComposeView(requireContext()).apply{

           setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
           setContent {

               val state = contactViewModel.state.collectAsState()
               val contactIndex = arguments?.getInt("contactIndex")?.toInt()!!
               val contactData = state.value.contactList[contactIndex]
               var nameField by remember{ mutableStateOf(contactData.fName +" "+ contactData.lName ) }
               var phoneField by remember{ mutableStateOf(contactData.phone[0] ) }
               var emailField by remember{ mutableStateOf(contactData.email[0]) }
               var streetAddressField by remember{ mutableStateOf(contactData.address.streetAddress + ", " + contactData.address.city + ", "
               + contactData.address.state + ", " + contactData.address.zipCode
               ) }

               Column(modifier = Modifier.fillMaxWidth().background(Color.DarkGray)) {
                   TextField(value = nameField, onValueChange = { nameField = it }, label = { Text("Name") })
                   TextField(value = phoneField, onValueChange = { phoneField= it }, label = { Text("Phone Number") })
                   TextField(value = emailField, onValueChange = { emailField= it }, label= { Text("Email") })
                   TextField(value = streetAddressField, onValueChange ={streetAddressField = it}, label = { Text("Address") } )

                   Button(onClick= {
                       var nameData = nameField.split(" ")
                       val fName = nameData[0]
                       val lName = nameData[1]
                       val phoneNum = phoneField
                       val emailStr = emailField
                       var addressData = streetAddressField.split(",")
                       val addressStr = Address(addressData[0],addressData[1], addressData[2], addressData[3])
                       contactViewModel.updateContact( contactIndex, Contact(fName, lName, addressStr, listOf(phoneNum),listOf(emailStr)))
                       findNavController().popBackStack()
                   }){ Text(" Save Contact ")}

               }
           }
       }
    }

}