package com.example.createcontactsapp.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.substring
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.createcontactsapp.model.local.Contact


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContactItemBox( contactDataItem: Contact, goToDetails: () -> Unit ) {

    Card(modifier = Modifier.fillMaxWidth().clickable{ goToDetails() }.padding(5.dp),colors = CardDefaults.cardColors(Color(
        49,
        52,
        61,
        255
    )
    )) {
        Row(modifier = Modifier.padding(10.dp), verticalAlignment = Alignment.CenterVertically){

                Box( modifier=Modifier.size(50.dp).clip(CircleShape).background(Color.Green), contentAlignment = Alignment.Center)
                    {
                        Box( modifier=Modifier.size(45.dp).clip(CircleShape).background(Color.Gray), contentAlignment = Alignment.Center)
                        {
                            Text(contactDataItem.fName.substring(0,1) + " " + contactDataItem.lName.substring(0,1))
                        }
                    }
            Text(contactDataItem.fName + " " + contactDataItem.lName,fontSize = 20.sp, color = Color.White, fontWeight = FontWeight.Bold, modifier = Modifier.padding(10.dp))
        }
    }
}