package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.createcontactsapp.model.local.Address
import com.example.createcontactsapp.model.local.Contact
import com.example.createcontactsapp.viewmodel.ContactsAppViewModel


class ContactCreate : Fragment() {

    private val contactViewModel by activityViewModels<ContactsAppViewModel>()


    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_contact_create, container, false)
        return ComposeView(requireContext()).apply{

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent{
                //First Name
                var nameField by remember{ mutableStateOf("" ) }
                var phoneField by remember{ mutableStateOf("" )}
                var emailField by remember{ mutableStateOf("") }
                var streetAddressField by remember{ mutableStateOf("")}

                Column(modifier= Modifier
                    .background(Color.DarkGray)
                    .fillMaxWidth()
                    .fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally) {
                    TextField(value = nameField, onValueChange = { nameField = it }, label = { Text("Name") })
                    TextField(value = phoneField, onValueChange = { phoneField= it }, label = { Text("Phone Number") })
                    TextField(value = emailField, onValueChange = { emailField= it }, label= {Text("Email")})
                    TextField(value = streetAddressField, onValueChange ={streetAddressField = it}, label = {Text("Address")} )

                    Button(colors=ButtonDefaults.buttonColors(containerColor= Color(
                        132,
                        238,
                        99,
                        255
                    )
                    ),onClick= {
                        var nameData = nameField.split(" ")
                        val fName = nameData[0]
                        val lName = nameData[1]
                        val phoneNum = phoneField
                        val emailStr = emailField
                        var addressData = streetAddressField.split(",")
                        val addressStr = Address(addressData[0],addressData[1], addressData[2], addressData[3])
                        contactViewModel.updateContactList(Contact(fName, lName, addressStr, listOf(phoneNum),listOf(emailStr)))
                        findNavController().popBackStack()
                    }){ Text(" Save Contact ")}
                }

            }
            }
    }

}